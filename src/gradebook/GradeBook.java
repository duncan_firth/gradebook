package gradebook;

import gradebook.model.StudentModel;
import gradebook.model.IStudentModel;
import gradebook.presenter.StudentPresenter;
import gradebook.view.IStudentView;
import gradebook.view.StudentView;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Duncan
 */
public class GradeBook {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //create an instance of StudentModel instance - interface
        //
        IStudentModel ism = new StudentModel();
        //create an instance of StudentView instance - interface
        IStudentView  isv = new StudentView();
        // create an instance of StudentPresenter with access to model and view
        StudentPresenter sp = new StudentPresenter(isv,ism);
        // assign view access to StudentPresenter
        //presenter will update the view
        isv.bind(sp);
        
    }
    
}
