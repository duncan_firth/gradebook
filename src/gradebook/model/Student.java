/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gradebook.model;

/**
 *
 * @author Duncan
 */
public final class Student {
    
   private int studentID;
   private String name;
   private String phone;
   private double assOne;
   private double assTwo;
   private double exam;
   public String grade;

   //no argument constructor
    public Student() {
    }
   
   //parameter constructor

    public Student(int studentID, String name, String phone, double assOne,  double assTwo, double exam) {
        this.studentID = studentID;
        this.name = name;
        this.phone = phone;
        this.assOne = assOne;
        this.assTwo = assTwo;
        this.exam = exam;
        
        
    }

    public Student(Double assOne, Double assTwo, Double exam) {
        this.assOne = assOne;
        this.assTwo = assTwo;
        this.exam = exam;
        calculateGrade(assOne,assTwo,exam);
    }

    public int getStudentID() {
        return studentID;
    }

    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public double getAssOne() {
        return assOne;
    }

    public double getAssTwo() {
        return assTwo;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setAssOne(double assOne) {
        this.assOne = assOne;
    }

    public void setAssTwo(double assTwo) {
        this.assTwo = assTwo;
    }

    public double getExam() {
        return exam;
    }

   
    public void setExam(double exam) {
        this.exam = exam;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }
    

    
//assign the grade

    public void calculateGrade(double assOne, double assTwo, double exam) {
       double assignmentMarks1 = assOne;
       double assignmentMarks2 = assTwo;
       double examMarks = exam;
      
      

        double total=assignmentMarks1+assignmentMarks2+examMarks;

        if (assignmentMarks1==0 && assignmentMarks2==0 && examMarks==0.0) {

            grade = "AF";

        } else if (total>= 45 && total <50 && assignmentMarks1 >=10 &&assignmentMarks2>=20 && examMarks<20) {

            grade = "SE";

        } else if ( (total>= 45 && total <50 && assignmentMarks1 >=10 &&assignmentMarks2<20 && examMarks>20) || (total>= 45 && total <50 && assignmentMarks1 <10 &&assignmentMarks2>20 && examMarks>20)) {

            grade = "SA";

        } else if (total<50) {

            grade = "F";

        } else if (total>=50 && total<65) {

            grade = "P";

        } else if (total>=65 && total<75) {

            grade = "C";

        } else if (total>=75 && total<85) {

            grade = "D";

        } else if(total>=85 && total<=100)

            grade="HD";

        else

            grade="Error";
      
    }// end method calculateGrade
 
}
