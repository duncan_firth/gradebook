/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gradebook.model;

import java.util.List;

/**
 *
 * @author Duncan
 */
public interface IStudentModel {
    
    public List<Student> getAllStudents();
    public List<Student> getStudentsByName(String name);
    public List<Student> getStudentsByID(int id );
    public int addStudent(String name, String phone, Double assOne, Double assTwo, Double exam);
    public int updateStudent(int id, String name, String phone, Double assOne, Double assTwo, Double exam);
    public void close();

}
