/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gradebook.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Duncan
 */
public class StudentModel implements IStudentModel{
    
   private static final String URL = "jdbc:mysql://localhost:3306/gradebook";
   private static final String USERNAME = "root";
   private static final String PASSWORD = "";
   
   private Connection connection = null; // manages connection
   private PreparedStatement selectAllStudents = null; 
   private PreparedStatement selectStudentByName = null; 
   private PreparedStatement selectStudentByID = null; 
   private PreparedStatement insertNewStudent = null; 
   private PreparedStatement updateStudent = null;
   
    public StudentModel() {
        try 
      {
         connection = 
            DriverManager.getConnection( URL, USERNAME, PASSWORD);

         // create query that selects all entries in the table
         selectAllStudents = 
            connection.prepareStatement( "SELECT * FROM Students" );
         
         // create query that selects entries with a specific name
         selectStudentByName = connection.prepareStatement( 
            "SELECT * FROM Students WHERE name = ?" );
         // create query that selects entirs with a specific id
         selectStudentByID = connection.prepareStatement( 
            "SELECT * FROM Students WHERE StudentID = ?" );
         // create insert that adds a new entry into the database
         insertNewStudent = connection.prepareStatement( 
            "INSERT INTO Students " + 
            "( Name, Phone, Ass1, Ass2, Exam ) " + 
            "VALUES ( ?, ?, ?, ?, ? )" );
         //update a student
        updateStudent = connection.prepareStatement(
                "UPDATE students " +
                "SET NAME=?, PHONE=?, ASS1=?, ASS2=?, EXAM=? WHERE studentId = ?");
      } // end try
      catch ( SQLException sqlException )
      {
         sqlException.printStackTrace();
         System.exit( 1 );
      } // end catch
   } // end StudentModel constructor

   // select all of the students in the database
   public List< Student > getAllStudents()
   {
      List< Student> results = null;
      ResultSet resultSet = null;
      
      try 
      {
         // executeQuery returns ResultSet containing matching entries
         resultSet = selectAllStudents.executeQuery(); 
         results = new ArrayList< Student>();
         
         while ( resultSet.next() )
         {
            results.add( new Student(
               resultSet.getInt( "StudentID" ),
               resultSet.getString( "Name" ),
               resultSet.getString( "Phone" ),
               resultSet.getDouble( "ASS1" ),
               resultSet.getDouble( "ASS2" ),
               resultSet.getDouble( "Exam" )
                ) );
         } // end while
      } // end try
      catch ( SQLException sqlException )
      {
         sqlException.printStackTrace();         
      } // end catch
      finally
      {
         try 
         {
            resultSet.close();
         } // end try
         catch ( SQLException sqlException )
         {
            sqlException.printStackTrace();         
            close();
         } // end catch
      } // end finally
      
      return results;
   } // end method getAllStudents
   
   // select Student by name   
   public List< Student > getStudentsByName( String name )
   {
      List< Student > results = null;
      ResultSet resultSet = null;

      try 
      {
         selectStudentByName.setString( 1, name ); // specify last name

         // executeQuery returns ResultSet containing matching entries
         resultSet = selectStudentByName.executeQuery(); 

         results = new ArrayList< Student >();

         while ( resultSet.next() )
         {
            results.add( new Student(
               resultSet.getInt( "StudentID" ),
               resultSet.getString( "Name" ),
               resultSet.getString( "Phone" ),
               resultSet.getDouble( "ASS1" ),
               resultSet.getDouble( "ASS2" ),
               resultSet.getDouble( "Exam" )
                ) );
         } // end while
      } // end try
      catch ( SQLException sqlException )
      {
         sqlException.printStackTrace();
      } // end catch
      finally
      {
         try 
         {
            resultSet.close();
         } // end try
         catch ( SQLException sqlException )
         {
            sqlException.printStackTrace();         
            close();
         } // end catch
      } // end finally
      
      return results;
   } // end method getByName
   
   // select student by name   
   public List< Student > getStudentsByID( int id )
   {
      List< Student > results = null;
      ResultSet resultSet = null;

      try 
      {
         selectStudentByID.setInt( 1, id ); // specify id

         // executeQuery returns ResultSet containing matching entries
         resultSet = selectStudentByID.executeQuery(); 

         results = new ArrayList< Student >();

         while ( resultSet.next() )
         {
               results.add( new Student(
               resultSet.getInt( "StudentID" ),
               resultSet.getString( "Name" ),
               resultSet.getString( "Phone" ),
               resultSet.getDouble( "ASS1" ),
               resultSet.getDouble( "ASS2" ),
               resultSet.getDouble( "Exam" )
                ) );
         } // end while
      } // end try
      catch ( SQLException sqlException )
      {
         sqlException.printStackTrace();
      } // end catch
      finally
      {
         try 
         {
            resultSet.close();
         } // end try
         catch ( SQLException sqlException )
         {
            sqlException.printStackTrace();         
            close();
         } // end catch
      } // end finally
      
      return results;
   } // end method getByName
   



    // update student 
    public int updateStudent(int id, String name, String phone, Double ASS1, Double ASS2, Double Exam) {
        int result = 0;
        Student s = new Student(0,name,phone, ASS1, ASS2,Exam); //create new temp student object
        // set parameters, then execute updateStudent
           try 
      {
         updateStudent.setString( 1, name );
         updateStudent.setString( 2, phone );
         updateStudent.setDouble( 3, ASS1 );
         updateStudent.setDouble( 4, ASS2 );
         updateStudent.setDouble( 5, Exam );
         updateStudent.setDouble( 6, id );
         
         // insert the new entry; returns # of rows updated
         result = updateStudent.executeUpdate(); 
      } // end try
      catch ( SQLException sqlException )
      {
         sqlException.printStackTrace();
         close();
      } // end catch
      
      return result;

            
            
    } // end method updateStudent
   
   
   
   
  
// add a student entry
   public int addStudent( 
      String name, String phone, Double ASS1, Double ASS2, Double Exam)
   {
      int result = 0;
      
      // set parameters, then execute insertNewStudent
      try 
      {
         insertNewStudent.setString( 1, name );
         insertNewStudent.setString( 2, phone );
         insertNewStudent.setDouble( 3, ASS1 );
         insertNewStudent.setDouble( 4, ASS2 );
         insertNewStudent.setDouble( 5, Exam );
         // insert the new entry; returns # of rows updated
         result = insertNewStudent.executeUpdate(); 
      } // end try
      catch ( SQLException sqlException )
      {
         sqlException.printStackTrace();
         close();
      } // end catch
      
      return result;
   } // end method addStudent
   
   // close the database connection
   public void close()
   {
      try 
      {
         connection.close();
      } // end try
      catch ( SQLException sqlException )
      {
         sqlException.printStackTrace();
      } // end catch
   } // end method close

   

    public PreparedStatement getInsertNewStudent() {
        return insertNewStudent;
    }

    public void setInsertNewStudent(PreparedStatement insertNewStudent) {
        this.insertNewStudent = insertNewStudent;
    }

   
    
} // end class PersonModel
   
   
   
   
   

