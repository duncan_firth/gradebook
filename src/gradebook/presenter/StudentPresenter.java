/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gradebook.presenter;

import gradebook.model.IStudentModel;
import gradebook.model.Student;
import gradebook.view.IStudentView;
import gradebook.view.StudentView;
import java.util.List;

/**
 *
 * @author Duncan
 */
public class StudentPresenter {
    
    IStudentView view;
    IStudentModel model;
    List<Student> results;
    int currentEntryIndex;
    int numberOfEntries;
    Student currentEntry;
   
    
    
    
    public StudentPresenter(IStudentView isv, IStudentModel ism) {
      
        view = isv;
        model = ism;
        currentEntryIndex = 0;
        numberOfEntries = 0;
        results = null;
        currentEntry = null;
        
    }

    private void populateAllTextFields() {
        
        view.displayRecord(currentEntry);
        view.displayMaxAndCurrent(numberOfEntries, currentEntryIndex);
    }
    
   // handles call when previousButton is clicked
   public void showPrevious() {   
      currentEntryIndex--;
      // wrap around
      if ( currentEntryIndex < 0 )
         currentEntryIndex = numberOfEntries - 1;
      currentEntry = results.get( currentEntryIndex );
      
      populateAllTextFields();
   }
     // handles call when nextButton is clicked
   public void showNext() {
      currentEntryIndex++;
      // wrap around
      if ( currentEntryIndex >= numberOfEntries )
         currentEntryIndex = 0;
      currentEntry = results.get( currentEntryIndex );
      populateAllTextFields();
   }

   // handles call when queryButton is clicked
   public void performQueryById(int id) {
       
    
     results = model.getStudentsByID( id );
     
      numberOfEntries = results.size();
      if ( numberOfEntries != 0 ) {
         currentEntryIndex = 0;
         currentEntry = results.get( currentEntryIndex );
         populateAllTextFields();
         view.setBrowsing(true);
         
      } 
      else
        view.displayMessage("Not found");
   }
    public void performQueryByName(String n) {
       
    
     results = model.getStudentsByName(n);
     
      numberOfEntries = results.size();
      if ( numberOfEntries != 0 ) {
         currentEntryIndex = 0;
         currentEntry = results.get( currentEntryIndex );
         populateAllTextFields();
         view.setBrowsing(true);
         
      } 
      else
        view.displayMessage("Not found");
   }
   // handles call when browseButton is clicked
   public void browse() {
      try {
         results = model.getAllStudents();
         
         numberOfEntries = results.size();
         if(numberOfEntries ==0)
             view.displayMessage("No records to browse");
         if ( numberOfEntries != 0 ) {
            currentEntryIndex = 0;
            currentEntry = results.get( currentEntryIndex );
            populateAllTextFields();
            view.setBrowsing(true);
         }
      }
      
      catch ( Exception e ) {
         e.printStackTrace();
      }
   }

   // handles call when insertButton is clicked
   public void insertNewEntry(String name, String phone, Double assOne, Double assTwo, Double exam) {
      
      int result = model.addStudent(name, phone, assOne, assTwo, exam);
      if ( result == 1 )
          view.displayMessage("Student added");
      else
          view.displayMessage("Student not added error");
          
      //browse();
   }
   
   
      // handles call when updateButton is clicked
   public void updateStudent(int studentId, String name, String phone, Double assOne, Double assTwo, Double exam) {
      
      int result = model.updateStudent(studentId, name, phone, assOne, assTwo, exam);
      if ( result == 1 )
          view.displayMessage("Student updated");
      else
          view.displayMessage("Student not updated error");
          
   }
   
   // handles window closure
   public void close() {
      model.close();
   }
    
}
