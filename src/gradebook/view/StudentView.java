/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gradebook.view;

import gradebook.model.Student;
import gradebook.presenter.StudentPresenter;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Duncan
 */
public class StudentView extends JFrame implements IStudentView{
    
   
   // the presenter for this view
   private StudentPresenter presenter;
   public String grade;
   // GUI components
   private JButton browseButton;
   private JLabel phoneLabel;
   private JTextField phoneTextField;
   private JLabel nameLabel;
   private JTextField nameTextField;
   private JLabel idLabel;
   private JTextField idTextField;
   private JTextField indexTextField;
   private JLabel assOneLabel;
   private JTextField assOneTextField;
   private JLabel examLabel;
   private JTextField examTextField;
   private JLabel gradeLabel;
   private JTextField gradeTextField;
   private JTextField maxTextField;
   private JButton nextButton;
   private JLabel ofLabel;
   private JLabel assTwoLabel;
   private JTextField assTwoTextField;
   private JButton previousButton;
   private JButton queryByNameButton;
   private JButton queryByIDButton;
   private JLabel queryByNameLabel;
   private JLabel queryByIDLabel;
   private JPanel queryPanel;
   private JPanel navigatePanel;
   private JPanel displayPanel;
   private JTextField queryByNameTextField;
   private JTextField queryByIDTextField;
   private JButton insertButton;
   private JButton gradeButton;
   private JButton updateButton;
   // no-argument constructor
   public StudentView()
   {
      super( "Grade Book" ); 
      
      // create GUI
      navigatePanel = new JPanel();
      previousButton = new JButton();
      indexTextField = new JTextField( 2 );
      ofLabel = new JLabel();
      maxTextField = new JTextField( 2 );
      nextButton = new JButton();
      displayPanel = new JPanel();
      idLabel = new JLabel();
      idTextField = new JTextField( 10 );
      nameLabel = new JLabel();
      nameTextField = new JTextField( 10 );
      assOneLabel = new JLabel();
      assOneTextField = new JTextField( 10 );
      phoneLabel = new JLabel();
      phoneTextField = new JTextField( 10 );
      assTwoLabel = new JLabel();
      assTwoTextField = new JTextField( 10 );
      examLabel = new JLabel();
      examTextField = new JTextField( 10 );
      gradeLabel = new JLabel();
      gradeTextField = new JTextField( 10 );
      
      queryPanel = new JPanel();
      queryByNameLabel = new JLabel();
      queryByNameTextField = new JTextField( 10 );
      queryByIDLabel = new JLabel();
      queryByIDTextField = new JTextField( 10 );
      queryByNameButton = new JButton();
      queryByIDButton = new JButton();
      browseButton = new JButton();
      insertButton = new JButton();
      gradeButton = new JButton();  
      updateButton = new JButton(); 
      setLayout( new FlowLayout( FlowLayout.CENTER, 10, 10 ) );
      setSize( 600, 355 );
      setResizable( false );

      navigatePanel.setLayout(
         new BoxLayout( navigatePanel, BoxLayout.X_AXIS ) );

      previousButton.setText( "Previous" );
      
      previousButton.addActionListener(
         new ActionListener()
         {
            public void actionPerformed( ActionEvent evt )
            {
               previousButtonActionPerformed( evt );
            } // end method actionPerformed
         } // end anonymous inner class
      ); // end call to addActionListener

      navigatePanel.add( previousButton );
      navigatePanel.add( Box.createHorizontalStrut( 10 ) );

      indexTextField.setHorizontalAlignment(
         JTextField.CENTER );
      
      navigatePanel.add( indexTextField );
      navigatePanel.add( Box.createHorizontalStrut( 10 ) );

      ofLabel.setText( "of" );
      navigatePanel.add( ofLabel );
      navigatePanel.add( Box.createHorizontalStrut( 10 ) );

      maxTextField.setHorizontalAlignment(
         JTextField.CENTER );
      maxTextField.setEditable( false );
      navigatePanel.add( maxTextField );
      navigatePanel.add( Box.createHorizontalStrut( 10 ) );

      
      nextButton.setText( "Next" );
      nextButton.addActionListener(
         new ActionListener()
         {
            public void actionPerformed( ActionEvent evt )
            {
               nextButtonActionPerformed( evt );
            } // end method actionPerformed
         } // end anonymous inner class
      ); // end call to addActionListener

      navigatePanel.add( nextButton );
      add( navigatePanel );
      
      //set browsing to off
      setBrowsing(false);
      displayPanel.setLayout( new GridLayout( 7, 2, 4, 4 ) );

      idLabel.setText( "Student ID:" );
      displayPanel.add( idLabel );
      idTextField.setEditable( false );
      displayPanel.add( idTextField );

      nameLabel.setText( "Student Name:" );
      displayPanel.add( nameLabel );
      displayPanel.add( nameTextField );

      phoneLabel.setText( "Phone:" );
      displayPanel.add( phoneLabel );
      displayPanel.add( phoneTextField );
      
      assOneLabel.setText( "Assignment One:" );
      displayPanel.add( assOneLabel );
      displayPanel.add( assOneTextField );

      

      assTwoLabel.setText( "Assignment Two:" );
      displayPanel.add( assTwoLabel );
      displayPanel.add( assTwoTextField );
      add( displayPanel );

      examLabel.setText( "Exam:" );
      displayPanel.add( examLabel );
      displayPanel.add( examTextField );
      add( displayPanel );
      
        gradeLabel.setText( "Grade:" );
      displayPanel.add( gradeLabel );
      displayPanel.add( gradeTextField );
      add( displayPanel );
      
      queryPanel.setLayout( 
         new BoxLayout( queryPanel, BoxLayout.X_AXIS) );

      queryPanel.setBorder( BorderFactory.createTitledBorder(
         "Find an entry by name or id" ) );
      queryByNameLabel.setText( "Student Name:" );
      queryPanel.add( Box.createHorizontalStrut( 5 ) );
      queryPanel.add( queryByNameLabel );
      queryPanel.add( Box.createHorizontalStrut( 10 ) );
      queryPanel.add( queryByNameTextField );
      queryPanel.add( Box.createHorizontalStrut( 10 ) );
      queryByNameButton.setText( "Find Name" );
      queryPanel.add( queryByNameButton );
      
      queryByIDLabel.setText( "Student Id" );
      queryPanel.add( Box.createHorizontalStrut( 5 ) );
      queryPanel.add( queryByIDLabel );
      queryPanel.add( Box.createHorizontalStrut( 10 ) );
      queryPanel.add( queryByIDTextField );
      queryPanel.add( Box.createHorizontalStrut( 10 ) );
      queryByIDButton.setText( "Find ID" );
      queryPanel.add( queryByIDButton );
      queryByNameButton.addActionListener(
         new ActionListener()
         {
            public void actionPerformed( ActionEvent evt )
            {
               queryButtonActionPerformed( evt );
            } // end method actionPerformed
         } // end anonymous inner class
      ); // end call to addActionListener
      
      queryByIDButton.addActionListener(
         new ActionListener()
         {
            public void actionPerformed( ActionEvent evt )
            {
               queryButtonActionPerformed( evt );
            } // end method actionPerformed
         } // end anonymous inner class
      ); // end call to addActionListener
      
      
      queryPanel.add( Box.createHorizontalStrut( 5 ) );
      add( queryPanel );
      
      
      browseButton.setText( "Browse All Entries" );
      browseButton.addActionListener(
         new ActionListener()
         {
            public void actionPerformed( ActionEvent evt )
            {
               browseButtonActionPerformed( evt );
            } // end method actionPerformed
         } // end anonymous inner class
      ); // end call to addActionListener

      add( browseButton );

      insertButton.setText( "Insert New Entry" );
      insertButton.addActionListener(
         new ActionListener()
         {
            public void actionPerformed( ActionEvent evt )
            {
               insertButtonActionPerformed( evt );
            } // end method actionPerformed
         } // end anonymous inner class
      ); // end call to addActionListener

	   add( insertButton );
           
           
           
      gradeButton.setText( "Calculate Grade" );
      gradeButton.addActionListener(
         new ActionListener()
         {
            public void actionPerformed( ActionEvent evt )
            {
               gradeButtonActionPerformed( evt );
            } // end method actionPerformed
         } // end anonymous inner class
      ); // end call to addActionListener

        updateButton.setText( "Update" );
      updateButton.addActionListener(
         new ActionListener()
         {
            public void actionPerformed( ActionEvent evt )
            {
               updateButtonActionPerformed( evt );
            } // end method actionPerformed
         } // end anonymous inner class
      ); // end call to addActionListener
      
      
	   add( gradeButton );
           add( updateButton );
           
      addWindowListener( 
         new WindowAdapter() 
         {  
            public void windowClosing( WindowEvent evt )
            {
               presenter.close();
               System.exit( 0 );
            } // end method windowClosing
         } // end anonymous inner class
      ); // end call to addWindowListener
	
      
      setVisible( true );
   } // end no-argument constructor

   // set the presenter for this view
   public void bind( StudentPresenter pp) {
       presenter = pp;
   }

    // Event handlers
    
   // handles call when previousButton is clicked
   private void previousButtonActionPerformed( ActionEvent evt ) {
       gradeTextField.setText("");
       presenter.showPrevious();
   }

   // handles call when nextButton is clicked
   private void nextButtonActionPerformed( ActionEvent evt ) 
   {
       gradeTextField.setText("");
       presenter.showNext();
   } 

   // handles call when queryButton is clicked
   private void queryButtonActionPerformed( ActionEvent evt )
   {
       if(evt.getSource()==queryByIDButton)
       presenter.performQueryById(Integer.parseInt(queryByIDTextField.getText()));
       else
       presenter.performQueryByName(queryByNameTextField.getText());
   } 

   // handles call when browseButton is clicked
   private void browseButtonActionPerformed( ActionEvent evt )
   {
       presenter.browse();
   }

   // handles call when insertButton is clicked
   private void insertButtonActionPerformed( ActionEvent evt ) 
   {
       String name=nameTextField.getText();
       String phone=phoneTextField.getText();
       Double assOne=Double.parseDouble(assOneTextField.getText());
       Double assTwo=Double.parseDouble(assTwoTextField.getText());
       Double exam=Double.parseDouble(examTextField.getText());
       presenter.insertNewEntry(name,phone,assOne,assTwo,exam);
   } 
    // handles call when gradeButton is clicked
   public String gradeButtonActionPerformed( ActionEvent evt ) 
   {
        
       String name=nameTextField.getText();
       String phone=phoneTextField.getText();
       Double assOne=Double.parseDouble(assOneTextField.getText());
       Double assTwo=Double.parseDouble(assTwoTextField.getText());
       Double exam=Double.parseDouble(examTextField.getText());
       //StudentCalculateGrades studentGrade = new StudentCalculateGrades(assOne,assTwo,exam);
       Student student = new Student(assOne,assTwo,exam);
       grade = student.grade;
       setGrade(student);
       Component frame = null;
       JOptionPane.showMessageDialog(frame, 
       "Student: "+name+"\n"+
       "Phone: "+phone+"\n"+
       "Assignment 1: "+assOne+"\n"+
       "Assignment 2: "+assTwo+"\n"+
       "Exam: "+exam+"\n"+
       "Grade: "+grade);
       return grade;
       
   } 

  
   // handles call when updateButton is clicked
   private void updateButtonActionPerformed( ActionEvent evt ) 
   {
      
       int studentID = Integer.parseInt(idTextField.getText());
       String name = nameTextField.getText();
       String phone = phoneTextField.getText();
       Double assOne = Double.parseDouble(assOneTextField.getText());
       Double assTwo = Double.parseDouble(assTwoTextField.getText());
       Double exam = Double.parseDouble(examTextField.getText());
       presenter.updateStudent(studentID, name,phone,assOne,assTwo,exam);
   } 

   
   public void setGrade(Student p)
   {
       gradeTextField.setText(""+p.getGrade());
   }
   
   
   
    // IStudentView interface implementation
   
    public void displayRecord(Student p)
    {
        
        idTextField.setText(""+p.getStudentID());
        nameTextField.setText( p.getName() );
        phoneTextField.setText(""+p.getPhone() );
        assOneTextField.setText(""+p.getAssOne());
        assTwoTextField.setText(""+p.getAssTwo());
        examTextField.setText(""+p.getExam());
    }
    public void displayMaxAndCurrent(int m,int c)
    {
        maxTextField.setText(""+m);
        indexTextField.setText(""+(c+1));
    }
    public void displayMessage(String m)
    {
         JOptionPane.showMessageDialog( this,m );
    }
    public void setBrowsing(Boolean f)
    {
        nextButton.setEnabled(f);
        previousButton.setEnabled( f );
    }

   
    
} // end class StudentView


