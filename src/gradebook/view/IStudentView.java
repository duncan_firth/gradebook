/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gradebook.view;

import gradebook.model.Student;
import gradebook.presenter.StudentPresenter;

/**
 *
 * @author Duncan
 */
public interface IStudentView {
    
    //IStudentView exposes the functionality of PatientView through these methods
    
    
    void bind(StudentPresenter p);
    void setBrowsing(Boolean f); 
    void displayMessage(String m); 
    void displayRecord(Student p); 
    void displayMaxAndCurrent(int m,int c);
    //void showMessageDialog( String s1, String s2 );
}
